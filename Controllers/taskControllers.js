const Task = require("../Models/task.js");

// Controllers and functions 

//Controller/function to get all the task on our database
module.exports.getAll = (request, response) => {

		Task.find({})
		// to capture the result of the find method
		.then(result => {
			return response.send(result)
		})
		//.catch method captures the error when the find method is executed.
		.catch(error => {
			return response.send(error);
		})

};


// Add Task on our database

module.exports.createTask = (request, response) => {
	const input = request.body;

	Task.findOne({name: input.name})
	.then(result => {
		if(result !== null){
			return response.send("The task is already existing!")
		}else{
			let newTask = new Task({
				name: input.name
			})
			newTask.save().then(save => {
				return response.send("The task is successfully added!")
			}).catch(error => {
				return response.send(error)
			})
		}
	})
	.catch(error => {
		return response.send(error);
	})
};

// Controller that will delete the document that contains the given objectID
module.exports.deleteTask = (request, response)=> {
	let idToBeDeleted = request.params.id;
	// findbyIdAndRemove to find the document that contains the id and then delete the document. 

	Task.findByIdAndRemove(idToBeDeleted)
	.then(result => {
		return response.send(result)
	})
	.catch(error => {
		return response.send(error);
	})
};


// Controller that will get specific task using id

module.exports.getOneById = (request,response) =>{
	const input = request.params.id;

	Task.findOne({_id: input})
	.then(result => {
				return response.send(result)
		})
	.catch(error => {
			return response.send(error)
		})
}

// Controller that will get specific task using name
module.exports.getOneByName = (request,response) =>{
	const input = request.params.name;

	Task.findOne({name: input})
	.then(result => {
			return response.send(result)
		})
	.catch(error => {
			return response.send(error)
		})
}

// Controller that will update the task to complete. 

module.exports.updateStatus = (request, response) => {
	const id = request.params.id;
	const status = request.params.status;

	// response.send(typeof status);
	// response.send(status);

	Task.findOne({_id: id})
		.then(result => {
			if (result !== null && status !== ""){
				Task.findByIdAndUpdate(id,{
					status : status	
				}, {new:true})
				.then(result => {
					return response.send(result)
				})

			}else{
				return response.send("Please ensure status code for update.")
			}

			})
		.catch(error => {
				return response.send(error)
			})



}


