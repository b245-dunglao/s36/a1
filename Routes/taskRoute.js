const express = require("express");
const router = express.Router(); 


const taskController = require("../Controllers/taskControllers.js")






/*routes*/

// Routes for getAll
router.get("/get", taskController.getAll);

// Route for createTask 
router.post("/addTask", taskController.createTask);

// Route for delete
router.delete("/deleteTasks/:id", taskController.deleteTask);

// Route for getting a specific task
router.get("/:id", taskController.getOneById);
router.get("/name/:name", taskController.getOneByName);

// Route for getting a specific task updated to complete. 
router.put("/:id/:status", taskController.updateStatus);


module.exports = router; 