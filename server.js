const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

const taskRoute = require("./Routes/taskRoute.js");

// MongoDB connection

mongoose.connect("mongodb+srv://admin:admin@b245-dunglao.tbtmdrd.mongodb.net/s35-discussion?retryWrites=true&w=majority", 
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Check connection

let db = mongoose.connection;

// error catcher

db.on("error", console.error.bind(console, "Connection Error!"));

// Confirmation of the connection
db.once("open",()=> console.log("We are now connected to the cloud!"));

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));





app.use("/tasks",taskRoute)


app.listen(port, () => console.log(`Server is running at port ${port}`));

/*

Separation of concerns : 

	1. Model should be connected to the controller.
		- taskController.js needs objects declared in Model(task.js) therefore require Model in taskController.js

	2. Controller should be connected to the routes. 
		- Routes(taskRoute.js) needs functions declared in Controller(taskController.js) therefore require Controller in routes

	3. Route should be connected to the server/application. 
		- Server(server.js) needs routes(get, post, put, delete methods) in Route(taskRoute.js) therefore require Route in server. 

		*/